<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Package stats</title>
    <meta charset="UTF-8">
    <script src="datatable.js" type="text/javascript" ></script>
    <style>
th {
    background-color: lightgrey;
    padding-left: 4px;
    padding-right: 6px;
    border: solid 1px black;
}
table {
    border-collapse: collapse;
    border: solid 1px black;
}
tr:hover {
    background-color: #FAF;
}
    </style>
  </head>
  <body>
    <p><a href="send_stats.sh">Download shell client to participate</a>, it only send pkglist, architecture and OpenBSD version</p>
    <p>Filtering is possible by architecture and/or os version with <strong>arch</strong> and <strong>osversion</strong> GET param on index.php</p>

<?php

include 'db.inc.php';

echo "<table id=\"listes\"><tr><th>Architectures known</th><th>OpenBSD versions</th></tr><tr><td>";
/* Here we display known archs */
$result = pg_query_params($dbconn, "SELECT sys_arch,count(sys_arch) from sys GROUP BY sys_arch ORDER BY sys_arch", array());
echo "<ul>";
while($row = pg_fetch_row($result)) {
  echo "<li><a href=\"index.php?arch=".$row[0]."\">".$row[0]."</a> (".$row[1].")</li>";
}
echo "</ul>";
echo "</td><td>";
/* Here we display known versions */
$result = pg_query_params($dbconn, "SELECT sys_version,count(sys_version) from sys GROUP BY sys_version ORDER BY sys_version", array());
while($row = pg_fetch_row($result)) {
  echo "<li><a href=\"index.php?osversion=".$row[0]."\">".$row[0]."</a> (".$row[1].")</li>";
}
echo "</ul>";
echo "</tr></table>";

/* HERE WE DISPLAY THE DATA */
$args = array();
$req  = "SELECT pkg_name,count(pkg_name) AS number from packages,sys where sys_id = pkg_sys ";

if (!empty($_GET['arch'])) {
  $args[] = filter_var($_GET['arch'], FILTER_SANITIZE_STRING);
  $req .= "and sys_arch = $" . count($args);
}
if (!empty($_GET['osversion'])) {
  $args[] = filter_var($_GET['osversion'], FILTER_SANITIZE_STRING);
  $req .= "and sys_version = $" . count($args);
}
$req .= " GROUP BY pkg_name ORDER BY pkg_name";

$result = pg_query_params($dbconn, $req, $args);

//print_r(pg_fetch_array($result));


if(pg_num_rows($result)) {
  echo "<h4>Packages installed ";
  if(!empty($_GET['arch'])) echo htmlspecialchars(filter_var($_GET['arch'], FILTER_SANITIZE_STRING));
  if(!empty($_GET['osversion'])) echo htmlspecialchars(filter_var($_GET['osversion'], FILTER_SANITIZE_STRING));
  echo " : </h4> (click on column name to sort)<table class=\"datatable\"><thead><tr><th>Package name</th><th>Count</th></tr></thead>";
  echo "<tbody>";
  while($row = pg_fetch_row($result)) {
    //    print_r($row);
    echo "<tr><td>".$row[0]."</td><td>".$row[1]."</td></tr>";
  }
  echo "</tbody></table>";
} else {
  echo "<h3>No data, maybe you typed a wrong param (unknown version/architecture)</h3>";
}

?>

    <script src="datatable.js" type="text/javascript" ></script>
    <script type="text/javascript">var datatables = datatable_autoload();</script>
</body>
</html>
